
package br.com.senac.ex7;


public class CalculadoraNota {
    
public class Calculadora {
    public static final String Aprovado = "Aprovado";
    public static final String Recuperacao = "Recuperacao";
    public static final String Reprovado = "Reprovado";
    
    public String calcular (double nota){
        
        if(nota >= 7){
            return Aprovado;
        }
        if(nota < 7 && nota >=4){
            return Recuperacao;
        }
        
        return Reprovado;
    }
    
    
}

}
